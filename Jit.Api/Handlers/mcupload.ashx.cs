﻿using Jit.Api.Bll;
using Jit.Api.Dom;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using TagLib;

namespace Jit.Api.Handlers
{
    /// <summary>
    /// Summary description for mcupload
    /// </summary>
    public class mcupload : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            HttpContext.Current.Response.AppendHeader("Access-Control-Allow-Origin", "*");
            var result = string.Empty;
            var op = HttpContext.Current.Request["op"];
            var tapeId = HttpContext.Current.Request["tapeId"];
            var mm = new musicassetteManager();

            if (HttpContext.Current.Request.Files.Count > 0)
            {
                try
                {
                    var path = HttpContext.Current.Server.MapPath("~/public/sound");
                    var file = HttpContext.Current.Request.Files[0];
                    var fileName = file.FileName;
                    var fileNameExt = Path.GetExtension(fileName).ToLower();

                    if (!string.IsNullOrEmpty(op) && op == "upajax")
                        fileName = HttpContext.Current.Request["videoId"] + ".mp3";

                    var audioUrl = Path.Combine(path, fileName);

                    file.SaveAs(audioUrl);

                    /*++++*/
                    if (string.IsNullOrEmpty(op))
                    {
                        var a = audioUrl;
                        var b = audioUrl.Replace(".3gp", ".mp3");
                        if (audioUrl.Contains(".wav"))
                            b = audioUrl.Replace(".wav", ".mp3");

                        Process proc = new Process();
                        proc.StartInfo.FileName = @"D:\inetpub\pixeldolphingames\lib\ffmpeg.exe";
                        proc.StartInfo.Arguments = "-i " + a + " " + b;
                        proc.StartInfo.RedirectStandardError = true;
                        proc.StartInfo.UseShellExecute = false;
                        proc.Start();
                        proc.WaitForExit(600000);
                        proc.Close();
                        if (System.IO.File.Exists(audioUrl.ToLower()))
                            System.IO.File.Delete(audioUrl.ToLower());
                    }
                    /*++++*/

                    result = "ok";

                    if (!string.IsNullOrEmpty(op) && op == "upajax")
                    {
                        var f = TagLib.File.Create(audioUrl, ReadStyle.Average);
                        var videoDuration = f.Properties.Duration.TotalMinutes.ToString();
                        var tapeFreeSpace = HttpContext.Current.Request["tapeFreeSpace"];
                        var recalcFree = Convert.ToDecimal(tapeFreeSpace) - Convert.ToDecimal(videoDuration);

                        if (recalcFree >= 0)
                        {
                            var song = new playlist();
                            song.tapeId = tapeId;
                            song.tapeType = HttpContext.Current.Request["tapeType"];
                            song.videoId = fileName.Replace(fileNameExt, "");
                            song.duration = videoDuration;
                            song.videoName = file.FileName.Replace(fileNameExt, "");
                            song.audioSrc = "sd";

                            result = mm.SetPlaylistSong(song);
                        }
                        else
                        {
                            if (System.IO.File.Exists(audioUrl.ToLower()))
                                System.IO.File.Delete(audioUrl.ToLower());
                        }
                    }
                }
                catch (Exception)
                {
                    result = "";
                }
            }

            context.Response.Write(result);
        }


        private string mediaUrl(string id, string ext)
        {
            var generator = new Random();
            var r = generator.Next(0, 1000000).ToString("D6");

            return id + "_" + r + ext;
        }



        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}