﻿using Jit.Api.Bll;
using Jit.Api.Dom;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace Jit.Api.Handlers
{
    /// <summary>
    /// Summary description for musicassette
    /// </summary>
    public class musicassette : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            HttpContext.Current.Response.AppendHeader("Access-Control-Allow-Origin", "*");
            var result = string.Empty;
            var op = HttpContext.Current.Request["op"];
            var token = HttpContext.Current.Request["token"];

            if (!string.IsNullOrEmpty(op))
            {
                if (op == "userchecktoken")
                {
                    var cm = new musicassetteManager();
                    result = JsonConvert.SerializeObject(cm.CheckUserToken(token));
                }
                else if (op == "signin")
                {
                    var cu = new musicassetteUser();
                    cu.email = HttpContext.Current.Request["email"];
                    cu.pwd = HttpContext.Current.Request["pwd"];

                    var cm = new musicassetteManager();
                    result = JsonConvert.SerializeObject(cm.SignIn(cu));
                }
                else if (op == "signup")
                {
                    var cu = new musicassetteUser();
                    cu.name = HttpContext.Current.Request["name"];
                    cu.email = HttpContext.Current.Request["email"];
                    cu.pwd = HttpContext.Current.Request["pwd"];

                    var cm = new musicassetteManager();
                    var cuLogged = new musicassetteUser();
                    cuLogged = cm.SignUp(cu);
                    result = JsonConvert.SerializeObject(cuLogged);

                    if (!string.IsNullOrEmpty(cuLogged.token) && !string.IsNullOrEmpty(cuLogged.code))
                    {
                        var mm = new EmailMessage();
                        mm.From = "staff@musicassette.co";
                        //mm.To = "crag000@gmail.com";
                        mm.To = cu.email;
                        var body = new StringBuilder();
                        mm.Subject = "Musicassette codice di attivazione";
                        var postmarkHtml = new PostmarkHtml();
                        var bHtml = postmarkHtml.mailBody("Il tuo codice attivazione è: " + cuLogged.code);
                        mm.HtmlBody = body.Append(bHtml).ToString();
                        mm.TextBody = "Il tuo codice attivazione è: " + cuLogged.code;

                        var pm = new postmarkEmail();
                        pm.Send(mm);
                    }
                }
                else if (op == "activation")
                {
                    var code = HttpContext.Current.Request["code"];
                    var cm = new musicassetteManager();
                    result = cm.CodeActivation(token, code);
                }
                else if (op == "pwdreset")
                {
                    var cu = new musicassetteUser();
                    cu.email = HttpContext.Current.Request["email"];
                    var cm = new musicassetteManager();
                    var pwd = cm.PwdReset(cu);
                    if (!string.IsNullOrEmpty(pwd))
                    {
                        var mm = new EmailMessage();
                        mm.From = "staff@musicassette.co";
                        //mm.To = "crag000@gmail.com";
                        mm.To = cu.email;
                        var body = new StringBuilder();
                        mm.Subject = "Musicassette recupero password";
                        var postmarkHtml = new PostmarkHtml();
                        var bHtml = postmarkHtml.mailBody("La tua password è: " + pwd);
                        mm.HtmlBody = body.Append(bHtml).ToString();
                        mm.TextBody = "La tua password è: " + pwd;

                        var pm = new postmarkEmail();
                        pm.Send(mm);

                        result = "1";
                    }
                }
                else if (op == "codereset")
                {
                    var cu = new musicassetteUser();
                    cu.email = HttpContext.Current.Request["email"];
                    var cm = new musicassetteManager();
                    var code = cm.CodeReset(cu);
                    if (!string.IsNullOrEmpty(code))
                    {
                        var mm = new EmailMessage();
                        mm.From = "staff@musicassette.co";
                        //mm.To = "crag000@gmail.com";
                        mm.To = cu.email;
                        var body = new StringBuilder();
                        mm.Subject = "Musicassette codice di attivazione";
                        var postmarkHtml = new PostmarkHtml();
                        var bHtml = postmarkHtml.mailBody("Il tuo codice attivazione è: " + code);
                        mm.HtmlBody = body.Append(bHtml).ToString();
                        mm.TextBody = "Il tuo codice attivazione è: " + code;

                        var pm = new postmarkEmail();
                        pm.Send(mm);

                        result = "1";
                    }
                }
                else if (op == "gettapebynfc")
                {
                    var nfcId = HttpContext.Current.Request["tagId"];
                    var mm = new musicassetteManager();
                    result = mm.GetTapeByNfc(nfcId, token);
                }
                else if (op == "setmixtapetitle")
                {
                    var title = HttpContext.Current.Request["title"];
                    var tapeid = HttpContext.Current.Request["tapeid"];

                    var mm = new musicassetteManager();
                    mm.SetMixtapeTitle(token, title, tapeid);
                }
                else if (op == "isownermixtape")
                {
                    var tapeid = HttpContext.Current.Request["tapeid"];

                    var mm = new musicassetteManager();
                    result = mm.MixtaperOwner(token, tapeid);
                }
                if (op == "step1")
                {
                    var mt = new mixtape
                    {
                        name = HttpContext.Current.Request["tapeName"],
                        userId = HttpContext.Current.Request["userToken"],
                        coverId = HttpContext.Current.Request["tapeCoverId"]
                    };

                    if (!string.IsNullOrEmpty(mt.name) && !string.IsNullOrEmpty(mt.userId) && !string.IsNullOrEmpty(mt.coverId))
                    {
                        var mm = new musicassetteManager();
                        result = JsonConvert.SerializeObject(mm.SetStep1(mt));
                    }
                }
                else if (op == "step1update")
                {
                    var mt = new mixtape
                    {
                        id = HttpContext.Current.Request["tapeId"],
                        name = HttpContext.Current.Request["tapeName"],
                        userId = HttpContext.Current.Request["userToken"],
                        coverId = HttpContext.Current.Request["tapeCoverId"]
                    };

                    if (!string.IsNullOrEmpty(mt.id) && !string.IsNullOrEmpty(mt.name) && !string.IsNullOrEmpty(mt.userId) && !string.IsNullOrEmpty(mt.coverId))
                    {
                        var mm = new musicassetteManager();
                        result = JsonConvert.SerializeObject(mm.SetStep1Update(mt));
                    }
                }
                else if (op == "checktapeExists")
                {
                    var mt = new mixtape
                    {
                        name = HttpContext.Current.Request["tapeName"],
                        userId = HttpContext.Current.Request["userToken"]
                    };

                    if (!string.IsNullOrEmpty(mt.name) && !string.IsNullOrEmpty(mt.userId))
                    {
                        var mm = new musicassetteManager();
                        result = JsonConvert.SerializeObject(mm.MixTapeExists(mt));
                    }
                }
                else if (op == "tapehead")
                {
                    var tapeId = HttpContext.Current.Request["tapeId"];
                    if (!string.IsNullOrEmpty(tapeId))
                    {
                        var mm = new musicassetteManager();
                        result = JsonConvert.SerializeObject(mm.GetTapeHead(tapeId));
                    }
                }
                else if (op == "setplaylist")
                {
                    var song = new playlist();
                    song.tapeId = HttpContext.Current.Request["tapeId"];
                    song.tapeType = HttpContext.Current.Request["tapeType"];
                    song.videoId = HttpContext.Current.Request["videoId"];
                    song.duration = HttpContext.Current.Request["duration"];
                    song.videoName = HttpContext.Current.Request["videoName"];
                    song.audioSrc = HttpContext.Current.Request["audioSrc"];

                    var mm = new musicassetteManager();
                    result = JsonConvert.SerializeObject(mm.SetPlaylistSong(song));
                }
                else if (op == "getplaylist")
                {
                    var tapeId = HttpContext.Current.Request["tapeId"];
                    var tapeType = HttpContext.Current.Request["tapeType"];
                    var mm = new musicassetteManager();
                    result = JsonConvert.SerializeObject(mm.GetPlaylistSong(tapeId, tapeType));
                }
                else if (op == "getfreespace")
                {
                    var videoDuration = "0";
                    if (!string.IsNullOrEmpty(HttpContext.Current.Request["videoDuration"]))
                        videoDuration = HttpContext.Current.Request["videoDuration"];

                    var side = "1";
                    if (!string.IsNullOrEmpty(HttpContext.Current.Request["tapeType"]))
                        side = HttpContext.Current.Request["tapeType"];

                    var mt = new mixtape();
                    mt.id = HttpContext.Current.Request["tapeId"];
                    mt.duration = HttpContext.Current.Request["tapeDuration"];
                    mt.side = side;

                    var mm = new musicassetteManager();
                    result = JsonConvert.SerializeObject(mm.GetFreeSpace(mt, videoDuration));
                }
                else if (op == "videoembeddable")
                {
                    var videoId = HttpContext.Current.Request["videoId"];
                    WebClient objWebClient = null;
                    UTF8Encoding objUTF8 = new UTF8Encoding();
                    var videostatus = string.Empty;
                    result = "true";
                    try
                    {
                        objWebClient = new WebClient();
                        videostatus = objWebClient.DownloadString("http://www.youtube.com/get_video_info?video_id=" + videoId);
                        if (videostatus.Contains("errorcode"))
                            result = "false";
                    }
                    catch (Exception) { }
                }
                else if (op == "videoembeddablemassive")
                {
                    var v = new video();
                    v.id = HttpContext.Current.Request["videoId"];
                    WebClient objWebClient = null;
                    UTF8Encoding objUTF8 = new UTF8Encoding();
                    var videostatus = string.Empty;
                    result = "true";
                    try
                    {
                        objWebClient = new WebClient();
                        videostatus = objWebClient.DownloadString("http://www.youtube.com/get_video_info?video_id=" + v.id);
                        if (videostatus.Contains("errorcode"))
                            v.isEmbeddable = "false";
                        else
                            v.isEmbeddable = "true";
                    }
                    catch (Exception) { }
                    result = JsonConvert.SerializeObject(v);
                }
                else if (op == "checksongduplicate")
                {
                    var videoId = HttpContext.Current.Request["videoId"];
                    var side = "1";
                    if (!string.IsNullOrEmpty(HttpContext.Current.Request["tapeType"]))
                        side = HttpContext.Current.Request["tapeType"];

                    var mt = new mixtape();
                    mt.id = HttpContext.Current.Request["tapeId"];
                    mt.side = side;

                    var mm = new musicassetteManager();
                    result = mm.GetSongDuplicate(mt, videoId);
                }
                else if (op == "getmixtapeme")
                {
                    var mm = new musicassetteManager();
                    result = JsonConvert.SerializeObject(mm.GetMixtapeMe(token));
                }
                else if (op == "checktapeactivation")
                {
                    var activationCode = HttpContext.Current.Request["tapeActivationCode"];
                    var mm = new musicassetteManager();
                    result = JsonConvert.SerializeObject(mm.CheckActivationCode(activationCode));
                }
                else if (op == "activatetapenfc")
                {
                    var nfcTapeId = HttpContext.Current.Request["nfcTapeId"];
                    var tapeId = HttpContext.Current.Request["tapeId"];
                    var mm = new musicassetteManager();
                    mm.ActivateMixTape(nfcTapeId, tapeId);
                }
                else if (op == "checktrackupload")
                {
                    var videoId = HttpContext.Current.Request["videoId"];
                    var mm = new musicassetteManager();
                    result = mm.CheckUploadTrack(videoId);
                }
                else if (op == "settrackupload")
                {
                    var tapeId = HttpContext.Current.Request["tapeId"];
                    var videoId = HttpContext.Current.Request["videoId"];
                    var videoName = HttpContext.Current.Request["videoName"];
                    var mm = new musicassetteManager();
                    mm.SetUploadTrackName(tapeId, videoId, videoName);
                    result = "ok";
                }
                else if (op == "deletetrack")
                {
                    var idTrack = HttpContext.Current.Request["idTrack"];

                    var mm = new musicassetteManager();
                    mm.DeleteTapeTrack(idTrack, token);
                    result = "ok";
                }
                else if (op == "addnl")
                {
                    var email = HttpContext.Current.Request["email"];
                    if (!string.IsNullOrEmpty(email))
                    {
                        var am = new musicassetteManager();
                        am.addToNl(email);
                    }
                }
                else if (op == "getbannerplayer")
                {
                    var tapeId = HttpContext.Current.Request["tapeId"];
                    var mm = new musicassetteManager();
                    result = JsonConvert.SerializeObject(mm.GetBannerPlayer(tapeId));
                }
            }

            context.Response.Write(result);
        }


        private string mediaUrl(string id, string ext)
        {
            var generator = new Random();
            var r = generator.Next(0, 1000000).ToString("D6");

            return id + "_" + r + ext;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}