﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Jit.Api.Infrastructure
{
    public static class Utility
    {
        public static void setCookie(string name, string value, int duration)
        {
            var cookieDomain = ConfigurationManager.AppSettings["httpCookieDomain"];
            var myCookie = new HttpCookie(name);
            myCookie.Value = value;
            if (!cookieDomain.Contains("localhost"))
                myCookie.Domain = cookieDomain;

            HttpContext.Current.Response.Cookies.Add(myCookie);

            if (duration > 0)
                myCookie.Expires = DateTime.UtcNow.AddDays(duration);
            else
                myCookie.Expires = DateTime.UtcNow.AddHours(1);
        }

        public static string getCookie(string name, string type)
        {
            string result = string.Empty;

            if (type == "request")
                if (HttpContext.Current.Request.Cookies[name] != null && !string.IsNullOrEmpty(HttpContext.Current.Request.Cookies[name].Value))
                    result = HttpContext.Current.Request.Cookies[name].Value;

            if (type == "response")
                if (HttpContext.Current.Response.Cookies[name] != null && !string.IsNullOrEmpty(HttpContext.Current.Response.Cookies[name].Value))
                    result = HttpContext.Current.Response.Cookies[name].Value;

            return result;
        }

        public static void deleteCookie(string name)
        {
            var cookieDomain = ConfigurationManager.AppSettings["httpCookieDomain"];
            var myCookie = new HttpCookie(name);
            myCookie.Value = "";
            if (!cookieDomain.Contains("localhost"))
                myCookie.Domain = cookieDomain;

            HttpContext.Current.Response.Cookies.Add(myCookie);
            myCookie.Expires = DateTime.UtcNow.AddDays(-1);
        }

        public static bool IsNumeric(string s)
        {
            float output;
            return float.TryParse(s, out output);
        }

        public static string CryptoPwd(string pwd)
        {
            var cryptoPwd = string.Empty;

            byte[] data = System.Text.Encoding.ASCII.GetBytes(pwd);
            data = new System.Security.Cryptography.SHA256Managed().ComputeHash(data);
            cryptoPwd = System.Text.Encoding.ASCII.GetString(data);

            return cryptoPwd;
        }

    }
}