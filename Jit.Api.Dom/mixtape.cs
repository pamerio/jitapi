﻿namespace Jit.Api.Dom
{
    public class mixtape
    {
        public string id { get; set; }
        public string name { get; set; }
        public string userId { get; set; }
        public string coverId { get; set; }
        public string duration { get; set; }
        public string side { get; set; }
        public string locked { get; set; }
    }
}
