﻿namespace Jit.Api.Dom
{
    public class banner
    {
        public string name { get; set; }
        public string link { get; set; }
        public string imgLink { get; set; }
        public string ytLink { get; set; }
        public string descr { get; set; }
    }
}
