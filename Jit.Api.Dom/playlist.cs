﻿namespace Jit.Api.Dom
{
    public class playlist
    {
        public string id { get; set; }
        public string tapeId { get; set; }
        public string tapeType { get; set; }
        public string videoId { get; set; }
        public string duration { get; set; }
        public string videoName { get; set; }
        public string audioSrc { get; set; }
    }
}
