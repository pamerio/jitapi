﻿namespace Jit.Api.Dom
{
    public class musicassetteUser
    {
        public string id { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string pwd { get; set; }
        public string token { get; set; }
        public string active { get; set; }
        public string code { get; set; }
    }
}

