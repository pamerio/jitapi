﻿using System.Text;

namespace Jit.Api.Bll
{
    public class PostmarkHtml
    {
        public string mailBody(string message)
        {
            var result = new StringBuilder();
            result.Append(@"<html xmlns='http://www.w3.org/1999/xhtml'>
                        <head>
                            <title>SonnyBono.com</title>
                        </head>
                        <body style='margin:0;background: #fff;'>
                            <div id='container' style='max-width:560px;margin:0 auto;border:10px solid #ffffff;'> 
                                <div style='background: #fff;margin-top:-15px;'>    
                                    <div id='logo' style='background: #fff;padding-top: 35px;height:50px;text-align:center;height:95px;cursor:pointer;'>
                                        <img src='http://musicassette.co/img/musicassettemail.jpg' style='border:0;' />
                                    </div>                   
                                    <div style='font-size: 18px;font-family: sans-serif;font-weight:bold;margin: 0 auto;text-align:center;color:#ffffff;padding: 30px;text-transform: uppercase;'>
                                       &nbsp;
                                    </div>
                                    <div style='color:#000000;padding-top:20px;text-align: center;background: #ffffff;margin: 0 auto!important;border: 0;text-transform: uppercase;'>
                                         " + message + @"
                                    </div>
                                </div></body></html>");
            return result.ToString();
        }
    }
}
