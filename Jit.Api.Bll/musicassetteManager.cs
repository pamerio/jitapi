﻿using Jit.Api.Dao;
using Jit.Api.Dom;
using System.Collections.Generic;

namespace Jit.Api.Bll
{
    public class musicassetteManager
    {
        public musicassetteUser CheckUserToken(string token)
        {
            var cu = new musicassetteUser();
            var cdao = new musicassetteDao();
            var userlogged = cdao.CheckToken(token);
            if (!string.IsNullOrEmpty(userlogged.active))
                cu.active = userlogged.active;

            return cu;
        }
        public musicassetteUser SignIn(musicassetteUser user)
        {
            var cu = new musicassetteUser();
            var ud = new musicassetteDao();
            var userlogged = ud.SignIn(user);
            if (!string.IsNullOrEmpty(userlogged.token))
            {
                cu.token = userlogged.token;
                cu.name = userlogged.name;
            }

            return cu;
        }
        public musicassetteUser SignUp(musicassetteUser user)
        {
            var cu = new musicassetteUser();
            var ud = new musicassetteDao();
            var userlogged = ud.SignUp(user);
            if (!string.IsNullOrEmpty(userlogged.token))
            {
                cu.token = userlogged.token;
                cu.code = userlogged.code;
            }


            return cu;
        }
        public string PwdReset(musicassetteUser user)
        {
            var ud = new musicassetteDao();
            return ud.PwdReset(user);
        }
        public string CodeReset(musicassetteUser user)
        {
            var ud = new musicassetteDao();
            return ud.CodeReset(user);
        }
        public string CodeActivation(string token, string code)
        {
            var ud = new musicassetteDao();
            return ud.CodeVerify(token, code);
        }
        public void SetMixtapeTitle(string token, string title, string tapeid)
        {
            var ud = new musicassetteDao();
            ud.MixtapeTitleUpdate(token, title, tapeid);
        }
        public string MixtaperOwner(string token, string tapeid)
        {
            var ud = new musicassetteDao();
            return ud.MixtapeIsOwner(token, tapeid);
        }
        public string GetTapeByNfc(string nfcId, string token)
        {
            var mdao = new musicassetteDao();
            return mdao.GetTapeByNfcId(nfcId, token);
        }
        public string SetStep1(mixtape tape)
        {
            var mdao = new musicassetteDao();
            return mdao.SetStep1(tape);
        }

        public string SetStep1Update(mixtape tape)
        {
            var mdao = new musicassetteDao();
            return mdao.UpdateStep1(tape);
        }

        public string MixTapeExists(mixtape tape)
        {
            var mdao = new musicassetteDao();
            return mdao.CheckMixTapeExists(tape);
        }


        public mixtape GetTapeHead(string tapeid)
        {
            var mdao = new musicassetteDao();
            return mdao.GetTapeHead(tapeid);
        }

        public string SetPlaylistSong(playlist song)
        {
            var mdao = new musicassetteDao();
            return mdao.SetPlaylist(song);
        }

        public List<playlist> GetPlaylistSong(string tapeId, string tapeType)
        {
            var mdao = new musicassetteDao();
            return mdao.GetPlaylist(tapeId, tapeType);
        }

        public string GetFreeSpace(mixtape tape, string videoDuration)
        {
            var mdao = new musicassetteDao();
            return mdao.GetTapeFreeSpace(tape, videoDuration);
        }

        public string GetSongDuplicate(mixtape tape, string videoId)
        {
            var mdao = new musicassetteDao();
            return mdao.CheckTapeSongDuplicate(tape, videoId);
        }

        public List<mixtape> GetMixtapeMe(string userId)
        {
            var mdao = new musicassetteDao();
            return mdao.GetMixtapeMelist(userId);
        }

        public nfcMixtape CheckActivationCode(string activationCode)
        {
            var mdao = new musicassetteDao();
            return mdao.CheckTapeActivationCode(activationCode);
        }

        public void ActivateMixTape(string nfcTapeId, string tapeId)
        {
            var mdao = new musicassetteDao();
            mdao.ActivateNfcMixtape(nfcTapeId, tapeId);
        }

        public string CheckUploadTrack(string videoId)
        {
            var mdao = new musicassetteDao();
            return mdao.CheckTrackUpload(videoId);
        }

        public void SetUploadTrackName(string tapeId, string videoId, string videoName)
        {
            var mdao = new musicassetteDao();
            mdao.SetTrackName(tapeId, videoId, videoName);
        }

        public void DeleteTapeTrack(string idTrack, string token)
        {
            var mdao = new musicassetteDao();
            mdao.DeleteTrack(idTrack, token);
        }
        public void addToNl(string email)
        {
            var ad = new musicassetteDao();
            ad.ContactsAdd(email);
        }
        public void addToTracer(string path)
        {
            var ad = new musicassetteDao();
            ad.TracerAdd(path);
        }
        public banner GetBannerPlayer(string tapeId)
        {
            var mdao = new musicassetteDao();
            return mdao.GetBaner(tapeId);
        }
    }
}
