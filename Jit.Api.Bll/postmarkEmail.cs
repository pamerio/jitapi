﻿using Jit.Api.Dom;
using System;
using System.Net.Mail;

namespace Jit.Api.Bll
{
    /// <summary>
    /// Summary description for emailPostmark
    /// </summary>
    public class postmarkEmail
    {
        public bool Send(EmailMessage message)
        {
            try
            {
                // setup email header
                var _MailMessage = new MailMessage();

                // Set the message sender
                // sets the from address for this e-mail message. 
                _MailMessage.From = new MailAddress(message.From, message.From);
                // Sets the address collection that contains the recipients of this e-mail message. 
                _MailMessage.To.Add(new MailAddress(message.To, message.To));

                if (!string.IsNullOrEmpty(message.Cc))
                    _MailMessage.CC.Add(new MailAddress(message.Cc, message.Cc));
                // sets the message subject.
                _MailMessage.Subject = message.Subject;
                // sets the message body. 
                _MailMessage.Body = message.HtmlBody;
                // sets a value indicating whether the mail message body is in Html. 
                // if this is false then ContentType of the Body content is "text/plain". 
                _MailMessage.IsBodyHtml = true;

                //// add all the file attachments if we have any
                //if (_Attachments != null && _Attachments.Length > 0)
                //    foreach (string _Attachment in _Attachments)
                //        _MailMessage.Attachments.Add(new System.Net.Mail.Attachment(_Attachment));

                // SmtpClient Class Allows applications to send e-mail by using the Simple Mail Transfer Protocol (SMTP).
                var _SmtpClient = new SmtpClient("smtp.postmarkapp.com");

                //Specifies how email messages are delivered. Here Email is sent through the network to an SMTP server.
                _SmtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;

                // Some SMTP server will require that you first authenticate against the server.
                // Provides credentials for password-based authentication schemes such as basic, digest, NTLM, and Kerberos authentication.
                var _NetworkCredential = new System.Net.NetworkCredential("2a5d8fda-8b4c-4e2f-901f-6507155b98b8", "2a5d8fda-8b4c-4e2f-901f-6507155b98b8");
                _SmtpClient.UseDefaultCredentials = false;
                _SmtpClient.Credentials = _NetworkCredential;

                //Let's send it
                _SmtpClient.Send(_MailMessage);

                // Do cleanup
                _MailMessage.Dispose();
                _SmtpClient = null;
            }
            catch (Exception _Exception)
            {
                var a = _Exception.Message;
                return false;
            }

            return false;
        }

    }
}
