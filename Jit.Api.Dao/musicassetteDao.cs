﻿using Jit.Api.Dom;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Jit.Api.Dao
{
    public class musicassetteDao
    {
        private readonly string ConnectionString;

        public musicassetteDao()
        {
            ConnectionString = ConfigurationManager.AppSettings["cs"];
        }

        public musicassetteUser CheckToken(string token)
        {
            var userLogged = new musicassetteUser();
            SqlDataReader dr;
            var connection = new SqlConnection(ConnectionString);
            connection.Open();

            var sp = "dbo.mc_web_check_token";
            var cmd = new SqlCommand(sp, connection);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@token", SqlDbType.VarChar, 50);
                cmd.Parameters["@token"].Value = token;
                dr = cmd.ExecuteReader();
                if (dr.Read())
                    userLogged.active = dr[0].ToString();
            }
            catch (Exception)
            {
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                cmd.Dispose();
            }
            return userLogged;
        }
        public musicassetteUser SignIn(musicassetteUser user)
        {
            var userLogged = new musicassetteUser();
            SqlDataReader dr;
            var connection = new SqlConnection(ConnectionString);
            connection.Open();

            var sp = "dbo.mc_user_signin";
            var cmd = new SqlCommand(sp, connection);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@email", SqlDbType.VarChar, 100);
                cmd.Parameters["@email"].Value = user.email;
                cmd.Parameters.Add("@pwd", SqlDbType.VarChar, 50);
                cmd.Parameters["@pwd"].Value = user.pwd;

                dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    userLogged.token = dr[0].ToString();
                    userLogged.name = dr[1].ToString();
                }
            }
            catch (Exception)
            {
                // ignored
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                cmd.Dispose();
            }
            return userLogged;
        }

        public musicassetteUser SignUp(musicassetteUser user)
        {
            var userLogged = new musicassetteUser();
            SqlDataReader dr;
            var connection = new SqlConnection(ConnectionString);
            connection.Open();

            var sp = "dbo.mc_user_signup";
            var cmd = new SqlCommand(sp, connection);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@name", SqlDbType.VarChar, 100);
                cmd.Parameters["@name"].Value = user.name;
                cmd.Parameters.Add("@email", SqlDbType.VarChar, 100);
                cmd.Parameters["@email"].Value = user.email;
                cmd.Parameters.Add("@pwd", SqlDbType.VarChar, 50);
                cmd.Parameters["@pwd"].Value = user.pwd;

                dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    userLogged.token = dr[0].ToString();
                    userLogged.code = dr[1].ToString();
                }
            }
            catch (Exception ex)
            {
                var a = ex.Message;
                // ignored
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                cmd.Dispose();
            }
            return userLogged;
        }
        public string PwdReset(musicassetteUser user)
        {
            var pwd = string.Empty;
            SqlDataReader dr;
            var connection = new SqlConnection(ConnectionString);
            connection.Open();

            var sp = "dbo.mc_user_reset_pwd";
            var cmd = new SqlCommand(sp, connection);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@email", SqlDbType.VarChar, 100);
                cmd.Parameters["@email"].Value = user.email;

                dr = cmd.ExecuteReader();
                if (dr.Read())
                    pwd = dr[0].ToString();
            }
            catch (Exception)
            {
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                cmd.Dispose();
            }
            return pwd;
        }
        public string CodeReset(musicassetteUser user)
        {
            var code = string.Empty;
            SqlDataReader dr;
            var connection = new SqlConnection(ConnectionString);
            connection.Open();

            var sp = "dbo.mc_user_reset_code";
            var cmd = new SqlCommand(sp, connection);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@email", SqlDbType.VarChar, 100);
                cmd.Parameters["@email"].Value = user.email;

                dr = cmd.ExecuteReader();
                if (dr.Read())
                    code = dr[0].ToString();
            }
            catch (Exception)
            {
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                cmd.Dispose();
            }
            return code;
        }
        public string CodeVerify(string token, string code)
        {
            var result = string.Empty;
            SqlDataReader dr;
            var connection = new SqlConnection(ConnectionString);
            connection.Open();

            var sp = "dbo.mc_user_code_verify";
            var cmd = new SqlCommand(sp, connection);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@token", SqlDbType.VarChar, 50);
                cmd.Parameters["@token"].Value = token;
                cmd.Parameters.Add("@code", SqlDbType.Int);
                cmd.Parameters["@code"].Value = Convert.ToInt32(code);

                dr = cmd.ExecuteReader();
                if (dr.Read())
                    result = dr[0].ToString();
            }
            catch (Exception)
            {
                // ignored
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                cmd.Dispose();
            }
            return result;
        }
        public void MixtapeTitleUpdate(string token, string title, string tapeid)
        {
            var connection = new SqlConnection(ConnectionString);
            connection.Open();

            var sp = "dbo.mc_set_tape_title";
            var cmd = new SqlCommand(sp, connection);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@name", SqlDbType.VarChar, 50);
                cmd.Parameters["@name"].Value = title;
                cmd.Parameters.Add("@token", SqlDbType.VarChar, 50);
                cmd.Parameters["@token"].Value = token;
                cmd.Parameters.Add("@tapeid", SqlDbType.Int);
                cmd.Parameters["@tapeid"].Value = Convert.ToInt32(tapeid);
                cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
                // ignored
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                cmd.Dispose();
            }
        }
        public string MixtapeIsOwner(string token, string tapeid)
        {
            var result = string.Empty;
            SqlDataReader dr;
            var connection = new SqlConnection(ConnectionString);
            connection.Open();

            var sp = "dbo.mc_mixtape_isowner";
            var cmd = new SqlCommand(sp, connection);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@token", SqlDbType.VarChar, 50);
                cmd.Parameters["@token"].Value = token;
                cmd.Parameters.Add("@tapeid", SqlDbType.Int);
                cmd.Parameters["@tapeid"].Value = Convert.ToInt32(tapeid);

                dr = cmd.ExecuteReader();
                if (dr.Read())
                    result = dr[0].ToString();
            }
            catch (Exception)
            {
                // ignored
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                cmd.Dispose();
            }
            return result;
        }
        public string SetStep1(mixtape tape)
        {
            var result = string.Empty;
            SqlDataReader dr;
            var connection = new SqlConnection(ConnectionString);
            connection.Open();

            var sp = "dbo.mc_create_tape_step1";
            var cmd = new SqlCommand(sp, connection);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@name", SqlDbType.VarChar, 50);
                cmd.Parameters["@name"].Value = tape.name;
                cmd.Parameters.Add("@userId", SqlDbType.Int);
                cmd.Parameters["@userId"].Value = Convert.ToInt32(tape.userId);
                cmd.Parameters.Add("@coverId", SqlDbType.Int);
                cmd.Parameters["@coverId"].Value = Convert.ToInt32(tape.coverId);
                dr = cmd.ExecuteReader();
                if (dr.Read())
                    result = dr[0].ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                cmd.Dispose();
            }
            return result;
        }

        public string UpdateStep1(mixtape tape)
        {
            var result = "1";
            var connection = new SqlConnection(ConnectionString);
            connection.Open();

            var sp = "dbo.mc_create_tape_step1update";
            var cmd = new SqlCommand(sp, connection);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@tapeId", SqlDbType.Int);
                cmd.Parameters["@tapeId"].Value = Convert.ToInt32(tape.id);
                cmd.Parameters.Add("@name", SqlDbType.VarChar, 50);
                cmd.Parameters["@name"].Value = tape.name;
                cmd.Parameters.Add("@userId", SqlDbType.Int);
                cmd.Parameters["@userId"].Value = Convert.ToInt32(tape.userId);
                cmd.Parameters.Add("@coverId", SqlDbType.Int);
                cmd.Parameters["@coverId"].Value = Convert.ToInt32(tape.coverId);
                cmd.ExecuteNonQuery();
            }
            catch
            {
                result = "";
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                cmd.Dispose();
            }
            return result;
        }

        public string CheckMixTapeExists(mixtape tape)
        {
            var result = "0";
            SqlDataReader dr;
            var connection = new SqlConnection(ConnectionString);
            connection.Open();

            var sp = "dbo.mc_create_tape_checkexists";
            var cmd = new SqlCommand(sp, connection);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@name", SqlDbType.VarChar, 50);
                cmd.Parameters["@name"].Value = tape.name;
                cmd.Parameters.Add("@userId", SqlDbType.Int);
                cmd.Parameters["@userId"].Value = Convert.ToInt32(tape.userId);
                dr = cmd.ExecuteReader();
                if (dr.Read())
                    result = "1";

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                cmd.Dispose();
            }
            return result;
        }

        public mixtape GetTapeHead(string tapeId)
        {
            var result = new mixtape();
            SqlDataReader dr;
            var connection = new SqlConnection(ConnectionString);
            connection.Open();

            var sp = "dbo.mc_get_tape_head";
            var cmd = new SqlCommand(sp, connection);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@tapeId", SqlDbType.Int);
                cmd.Parameters["@tapeId"].Value = tapeId;
                dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    result.name = dr[0].ToString();
                    result.coverId = dr[1].ToString();
                    result.duration = dr[2].ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                cmd.Dispose();
            }
            return result;
        }

        public string SetPlaylist(playlist song)
        {
            var result = string.Empty;
            var connection = new SqlConnection(ConnectionString);
            connection.Open();

            var sp = "dbo.mc_set_tape_playlist";
            var cmd = new SqlCommand(sp, connection);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@tapeId", SqlDbType.Int);
                cmd.Parameters["@tapeId"].Value = Convert.ToInt32(song.tapeId);
                cmd.Parameters.Add("@tapeType", SqlDbType.Int);
                cmd.Parameters["@tapeType"].Value = Convert.ToInt32(song.tapeType);
                cmd.Parameters.Add("@videoId", SqlDbType.VarChar, 50);
                cmd.Parameters["@videoId"].Value = song.videoId;
                cmd.Parameters.Add("@duration", SqlDbType.Decimal);
                cmd.Parameters["@duration"].Value = Convert.ToDecimal(song.duration.Replace(".", ","));
                cmd.Parameters.Add("@videoName", SqlDbType.VarChar, 100);
                cmd.Parameters["@videoName"].Value = song.videoName;
                cmd.Parameters.Add("@audioSrc ", SqlDbType.VarChar, 10);
                cmd.Parameters["@audioSrc "].Value = song.audioSrc;
                cmd.ExecuteNonQuery();

                result = "ok";
            }
            catch
            {
                result = "";
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                cmd.Dispose();
            }
            return result;
        }

        public List<playlist> GetPlaylist(string tapeId, string tapeType)
        {
            var results = new List<playlist>();
            SqlDataReader dr;
            var connection = new SqlConnection(ConnectionString);
            connection.Open();

            var sp = "dbo.mc_get_tape_playlist";
            var cmd = new SqlCommand(sp, connection);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@tapeId", SqlDbType.Int);
                cmd.Parameters["@tapeId"].Value = Convert.ToInt32(tapeId);
                cmd.Parameters.Add("@tapeType", SqlDbType.Int);
                cmd.Parameters["@tapeType"].Value = Convert.ToInt32(tapeType);
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    var result = new playlist();
                    result.id = dr[0].ToString();
                    result.videoId = dr[1].ToString();
                    result.videoName = dr[2].ToString();
                    result.duration = dr[3].ToString();
                    result.audioSrc = dr[4].ToString();

                    results.Add(result);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                cmd.Dispose();
            }
            return results;
        }

        public string GetTapeFreeSpace(mixtape tape, string videoDuration)
        {
            var result = string.Empty;
            SqlDataReader dr;
            var connection = new SqlConnection(ConnectionString);
            connection.Open();

            var sp = "dbo.mc_get_tape_freespace";
            var cmd = new SqlCommand(sp, connection);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@tapeId", SqlDbType.Int);
                cmd.Parameters["@tapeId"].Value = Convert.ToInt32(tape.id);
                cmd.Parameters.Add("@tapeDuration", SqlDbType.Decimal);
                cmd.Parameters["@tapeDuration"].Value = Convert.ToDecimal(tape.duration);
                cmd.Parameters.Add("@videoDuration", SqlDbType.Decimal);
                cmd.Parameters["@videoDuration"].Value = Convert.ToDecimal(videoDuration);
                cmd.Parameters.Add("@side", SqlDbType.Int);
                cmd.Parameters["@side"].Value = Convert.ToInt32(tape.side);
                dr = cmd.ExecuteReader();
                if (dr.Read())
                    result = dr[0].ToString();

                if (result == "")
                    result = tape.duration;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                cmd.Dispose();
            }
            return result;
        }

        public string CheckTapeSongDuplicate(mixtape tape, string videoId)
        {
            var result = "true";
            SqlDataReader dr;
            var connection = new SqlConnection(ConnectionString);
            connection.Open();

            var sp = "dbo.mc_check_tape_song_duplicate";
            var cmd = new SqlCommand(sp, connection);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@tapeId", SqlDbType.Int);
                cmd.Parameters["@tapeId"].Value = Convert.ToInt32(tape.id);
                cmd.Parameters.Add("@tapeType", SqlDbType.Int);
                cmd.Parameters["@tapeType"].Value = Convert.ToInt32(tape.side);
                cmd.Parameters.Add("@videoId", SqlDbType.VarChar, 50);
                cmd.Parameters["@videoId"].Value = videoId;
                dr = cmd.ExecuteReader();
                if (dr.Read())
                    result = "false";
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                cmd.Dispose();
            }
            return result;
        }

        public string GetTapeByNfcId(string nfcId, string token)
        {
            var result = string.Empty;
            SqlDataReader dr;
            var connection = new SqlConnection(ConnectionString);
            connection.Open();

            var sp = "dbo.mc_set_tape_bynfc";
            var cmd = new SqlCommand(sp, connection);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@token", SqlDbType.VarChar, 50);
                cmd.Parameters["@token"].Value = token;
                cmd.Parameters.Add("@nfcId", SqlDbType.VarChar, 50);
                cmd.Parameters["@nfcId"].Value = nfcId;
                dr = cmd.ExecuteReader();
                if (dr.Read())
                    result = dr[0].ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                cmd.Dispose();
            }
            return result;
        }

        public List<mixtape> GetMixtapeMelist(string token)
        {
            var results = new List<mixtape>();
            SqlDataReader dr;
            var connection = new SqlConnection(ConnectionString);
            connection.Open();

            var sp = "dbo.mc_get_mixtape_me";
            var cmd = new SqlCommand(sp, connection);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@token", SqlDbType.VarChar, 50);
                cmd.Parameters["@token"].Value = token;
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    var result = new mixtape();
                    result.id = dr[0].ToString();
                    result.name = dr[1].ToString();
                    result.coverId = dr[2].ToString();

                    results.Add(result);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                cmd.Dispose();
            }
            return results;
        }

        public nfcMixtape CheckTapeActivationCode(string activationCode)
        {
            var result = new nfcMixtape(); //not valid code
            SqlDataReader dr;
            var connection = new SqlConnection(ConnectionString);
            connection.Open();

            var sp = "dbo.mc_check_tape_activationcode";
            var cmd = new SqlCommand(sp, connection);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@activationCode", SqlDbType.VarChar, 10);
                cmd.Parameters["@activationCode"].Value = activationCode;
                dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    result.id = dr[0].ToString();
                    result.coverId = dr[1].ToString();
                    result.isactive = dr[2].ToString();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                cmd.Dispose();
            }
            return result;
        }

        public void ActivateNfcMixtape(string nfcTapeId, string tapeId)
        {
            var connection = new SqlConnection(ConnectionString);
            connection.Open();

            var sp = "dbo.mc_set_tape_activationcode";
            var cmd = new SqlCommand(sp, connection);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@nfcTapeId", SqlDbType.Int);
                cmd.Parameters["@nfcTapeId"].Value = nfcTapeId;
                cmd.Parameters.Add("@tapeId", SqlDbType.Int);
                cmd.Parameters["@tapeId"].Value = tapeId;
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                cmd.Dispose();
            }
        }

        public string CheckTrackUpload(string videoId)
        {
            var result = string.Empty;
            SqlDataReader dr;
            var connection = new SqlConnection(ConnectionString);
            connection.Open();

            var sp = "dbo.mc_check_track_upload";
            var cmd = new SqlCommand(sp, connection);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@videoId", SqlDbType.VarChar, 50);
                cmd.Parameters["@videoId"].Value = videoId;
                dr = cmd.ExecuteReader();
                if (dr.Read())
                    result = dr[0].ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                cmd.Dispose();
            }
            return result;
        }

        public void SetTrackName(string tapeId, string videoId, string videoName)
        {
            var connection = new SqlConnection(ConnectionString);
            connection.Open();

            var sp = "dbo.mc_set_trackname";
            var cmd = new SqlCommand(sp, connection);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@tapeId", SqlDbType.Int);
                cmd.Parameters["@tapeId"].Value = tapeId;
                cmd.Parameters.Add("@videoId", SqlDbType.VarChar, 50);
                cmd.Parameters["@videoId"].Value = videoId;
                cmd.Parameters.Add("@videoName", SqlDbType.VarChar, 100);
                cmd.Parameters["@videoName"].Value = videoName;
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                cmd.Dispose();
            }
        }
        public void DeleteTrack(string idTrack, string token)
        {
            var connection = new SqlConnection(ConnectionString);
            connection.Open();

            var sp = "dbo.mc_delete_track";
            var cmd = new SqlCommand(sp, connection);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@idTrack", SqlDbType.Int);
                cmd.Parameters["@idTrack"].Value = Convert.ToInt32(idTrack);
                cmd.Parameters.Add("@token", SqlDbType.VarChar, 50);
                cmd.Parameters["@token"].Value = token;
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                cmd.Dispose();
            }
        }
        public void ContactsAdd(string email)
        {
            var connection = new SqlConnection(ConnectionString);
            connection.Open();

            var sp = "dbo.mc_nl_mailinglist_create";
            var cmd = new SqlCommand(sp, connection);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@email", SqlDbType.VarChar, 100);
                cmd.Parameters["@email"].Value = email;
                cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
                // ignored
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                cmd.Dispose();
            }
        }
        public void TracerAdd(string path)
        {
            var connection = new SqlConnection(ConnectionString);
            connection.Open();

            var sp = "dbo.mc_set_tracer";
            var cmd = new SqlCommand(sp, connection);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@path", SqlDbType.VarChar, 250);
                cmd.Parameters["@path"].Value = path;
                cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
                // ignored
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                cmd.Dispose();
            }
        }
        public banner GetBaner(string tapeId)
        {
            var result = new banner(); //not valid code
            SqlDataReader dr;
            var connection = new SqlConnection(ConnectionString);
            connection.Open();

            var sp = "dbo.mc_get_banner";
            var cmd = new SqlCommand(sp, connection);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@tapeId", SqlDbType.Int);
                cmd.Parameters["@tapeId"].Value = tapeId;
                dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    result.name = dr[0].ToString();
                    result.imgLink = dr[1].ToString();
                    result.link = dr[2].ToString();
                    result.ytLink = dr[3].ToString();
                    result.descr = dr[4].ToString();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                cmd.Dispose();
            }
            return result;
        }
    }
}
